<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_pegawai',
        'npp',
        'nama_pegawai',
        'jenis_kelamin',
        'tgl_lahir',
        'nama_jabatan',
        'nama_unit_kerja',
        'status'
    ];
}
