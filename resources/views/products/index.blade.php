@extends('products.layouts')

@section('content')

<div class="row justify-content-center mt-3">
    <div class="col-md-12">

        @if ($message = Session::get('success'))
            <div class="alert alert-success" role="alert">
                {{ $message }}
            </div>
        @endif

        <div class="card">
            <div class="card-header">List Pegawai</div>
            <div class="card-body">
                <a href="{{ route('products.create') }}" class="btn btn-success btn-sm my-2"><i class="bi bi-plus-circle"></i> Tambahkan Pegawai</a>
                <table class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">NPP</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Jenis Kelamin</th>
                        <th scope="col">Tanggal Lahir</th>
                        <th scope="col">Jabatan</th>
                        <th scope="col">Unit Kerja</th>
                        <th scope="col">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                        @forelse ($products as $product)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $product->id_pegawai }}</td>
                            <td>{{ $product->npp }}</td>
                            <td>{{ $product->nama_pegawai }}</td>
                            <td>{{ $product->jenis_kelamin }}</td>
                            <td>{{ $product->tgl_lahir }}</td>
                            <td>{{ $product->nama_jabatan }}</td>
                            <td>{{ $product->nama_unit_kerja }}</td>
                            <td>{{ $product->status }}</td>
                            <td>
                                <form action="{{ route('products.destroy', $product->id_pegawai) }}" method="post">
                                    @csrf
                                    @method('DELETE')

                                    <a href="{{ route('products.show', $product->id_pegawai) }}" class="btn btn-warning btn-sm"><i class="bi bi-eye"></i> Show</a>

                                    <a href="{{ route('products.edit', $product->id_pegawai) }}" class="btn btn-primary btn-sm"><i class="bi bi-pencil-square"></i> Edit</a>

                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Do you want to delete this product?');"><i class="bi bi-trash"></i> Delete</button>
                                </form>
                            </td>
                        </tr>
                        @empty
                            <td colspan="7">
                                <span class="text-danger">
                                    <strong>No Pegawai Found!</strong>
                                </span>
                            </td>
                        @endforelse
                    </tbody>
                  </table>

                  {{ $products->links() }}

            </div>
        </div>
    </div>
</div>

@endsection
